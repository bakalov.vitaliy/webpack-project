import 'normalize.css/normalize.css'

import Vue from 'vue'
import App from './App.vue'

import './assets/stylesheets/index.scss'

new Vue({
  el: '#app',
  render: h => h(App)
})
