module.exports = {
  env: {
    browser: true,
    node: true
  },

  parserOptions: {
    parser: '@babel/eslint-parser'
  },

  extends: [
    'plugin:vue/recommended',
    'eslint:recommended'
  ],

  rules: {
    indent: ['error', 2, { SwitchCase: 1 }],
    semi: ['error', 'never'],
    quotes: ['error', 'single'],
    'max-len': ['warn', { code: 120 }],
    'comma-dangle': ['error', 'never'],
    'object-curly-spacing': ['error', 'always'],
    'arrow-parens': ['error', 'as-needed'],
    'linebreak-style': 0,
    'no-multiple-empty-lines': ['error', { max: 1 }]
  }
}
